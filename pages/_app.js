import { useState, useEffect } from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'//import boostrap
import AppNavbar from '../components/NavBar'; 
import {UserProvider} from '../contexts/UserContext'
import { Container } from 'react-bootstrap'; 
import AppHelper from '../app-helper';


function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({ email: null })

	useEffect(() => {
		if(AppHelper.getAccessToken !== null) {
			const options = {
				headers: { Authorization: `Bearer ${AppHelper.getAccessToken()}`}
			}
			fetch('https://cryptic-cliffs-04365.herokuapp.com/api/users/details', options).then((response) => response.json()).then((userData) => {
				//lets create a control structure to determine the response if a user has been authenticated
				if(typeof userData.email != 'undefined') {
					setUser({ email: userData.email })
				}else {
					//if the condition was not met meaning the userData is null or indefined
					setUser({email: null})
				}
			})
		}
	}, [user.id])

	const unsetUser = () => {
		//this function will be used to clear out contents of the local storage.
		localStorage.clear()
		setUser({ email: null })
	}
  return (
	<div className="main__container">
		<UserProvider value={ {user, setUser, unsetUser} }>
			<AppNavbar />
			<Container>
			  <Component {...pageProps} />
			</Container>
		</UserProvider>
	</div>
  )
}

export default MyApp
