import { useState, useEffect } from 'react'
import { Table, Button } from 'react-bootstrap'
import Link from 'next/link'
import View from '../../../components/View'
import AppHelper from '../../../app-helper'

export default function getCategory() {
   
    const [categories, setCategories] = useState([])

     useEffect(() => {
        const payload = {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }` 
            }
        }
        fetch('https://cryptic-cliffs-04365.herokuapp.com/api/users/get-categories', payload).then(res => res.json()).then(categories => {
                console.log(categories)
                let result = categories.map((category) => {
                    return (
                        <tr>
                            <td>{category.name}</td>
                            <td>{category.type}</td>
                        </tr>
                    )
                })
                setCategories(result)
        })


    }, [categories])

    return (
        <View title="Categories">
            <h3>Categories</h3>
            <Link href="/user/categories/new"><a className="btn btn-success mt-1 mb-3">Add</a></Link>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                 
                    {categories}
                 
                </tbody>
            </Table>
        </View>
    )
}
