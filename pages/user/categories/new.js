import { useState } from 'react'
import { Form, Button, Row, Col, Card } from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'
import View from '../../../components/View'
import AppHelper from '../../../app-helper'

export default function newCategory() {

    //start of new form category
    const NewCategoryForm = () => {
        const [categoryName, setCategoryName] = useState('')
        const [typeName, setTypeName] = useState(undefined)

            function createCategory(e) {
                e.preventDefault()

                fetch('https://cryptic-cliffs-04365.herokuapp.com/api/users/add-category', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${AppHelper.getAccessToken()}`
                    },
                    body: JSON.stringify({
                        name: categoryName,
                        typeName: typeName
                    })
                }).then(res => res.json()).then(data => {
                    console.log(data)
                    if(data === true){
                        Swal.fire({
                            icon: 'success',
                            title: 'Add New Category Successful',
                            text: 'New category successfully added.'
                        })
                    }else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Add New Category Error',
                            text: 'An error encountered while saving the new category.'
                        })
                    }
                })
            }
        

        return (
            <Form onSubmit={ (e) => createCategory(e) }>
                <Form.Group controlId="categoryName">
                    <Form.Label>Category Name:</Form.Label>
                    <Form.Control type="text" placeholder="Enter category name" value={ categoryName } onChange={ (e) => setCategoryName(e.target.value) } required/>
                </Form.Group>
                <Form.Group controlId="typeName">
                    <Form.Label>Category Type:</Form.Label>
                    <Form.Control as="select" value={ typeName } onChange={ (e) => setTypeName(e.target.value) } required>
                        <option value selected disabled>Select Category</option>
                        <option value="Income">Income</option>
                        <option value="Expense">Expense</option>
                    </Form.Control>
                </Form.Group>
                <Button variant="success" type="submit">Submit</Button>
            </Form>
        )
    } //end of new form category

    return (
        <View title="New Category">
            <Row className="justify-content-center">
                <Col xs md="6" className="newcategory__container">
                    <h3>Create New Category</h3>
                    <Card>
                        <Card.Header>Category Information</Card.Header>
                        <Card.Body>
                            <NewCategoryForm/>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </View>
    )
} //end



