import { useState, useContext } from 'react'
import { Form, Button, Card, Row, Col } from 'react-bootstrap'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import UserContext from '../contexts/UserContext'
import View from '../components/View'; 
import Swal from 'sweetalert2'
import Router from 'next/router'
import Link from 'next/link'

export default function Home() {
 const [email, setEmail] = useState("")
 const [password, setPassword] = useState("")
 const { user, setUser } = useContext(UserContext)

  if(user.email !== null) {
    Router.push('/user/records');
  }

 //lets create a function to retrieve the user details before the client can be redirected inside the records page.
 const retrieveUserDetails = (accessToken) => {
    //we have to make sure the user has already beed verified. Meaning, it was already granted access token.
    const options = {
      headers: {Authorization: `Bearer ${accessToken}`}
    }
    //create a request going to the desired endpoint with the payload.
    fetch('https://cryptic-cliffs-04365.herokuapp.com/api/users/details', options).then((response) => response.json()).then(data => {
      setUser({ email: data.email }) //lets modify this by acquiring the email property from the data response
      Router.push('/user/records') 
    })
 }

 function login(e) {
    e.preventDefault()
    //describe the request to login
    fetch('https://cryptic-cliffs-04365.herokuapp.com/api/users/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password
      }) //for the request to be accepted by API body should be in json format
    }).then(res => res.json()).then(data => {
      console.log(data)
      if(typeof data.accessToken !== "undefined") {
        //store the access token in the local storage
        localStorage.setItem('token', data.accessToken)
        retrieveUserDetails(data.accessToken)
        Swal.fire({
              icon: 'success',
              title: 'Successfuly logged in.'
        })
        
      } else {
        Swal.fire('Login Error', 'You may have registered using a different login procedure', 'error')
      }
    })
 }

  return (
      <View title={ 'CashFlow | Login' }>
        <Col md={{ span: 6, offset: 3 }} className="index__container">
          <h1 className="welcome__tag">Welcome to CashFlow!</h1>
          <p className="tagline"><strong>Manage your income. Track your expenses. Join CashFlow!</strong></p>
          <p>Login by using your Registered Email.</p>
          <Form onSubmit={e => login(e)} className="login__form">
              <Form.Group controlId="email">
                  <Form.Label>Email:</Form.Label>
                  <Form.Control type="text" value={email} onChange={e=>setEmail(e.target.value)} required/>
              </Form.Group>
              <Form.Group controlId="password">
                  <Form.Label>Password:</Form.Label>
                  <Form.Control type="password" value={password} onChange={e=>setPassword(e.target.value)} required/>
              </Form.Group>
              <Button type="submit" className="btn-block mb-3 btn__login"> Login </Button>
              <Link href="/register">
                <a className="btn__lgn__register">Not yet registered? Register Now</a>
              </Link>
          </Form>
        </Col>
      </View>
  )
}
