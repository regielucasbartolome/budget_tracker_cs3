import { Fragment, useState, useEffect, useContext } from 'react'; 
import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../contexts/UserContext' 

export default function AppNavBar() {
	const { user } = useContext(UserContext)
	const [isExpanded, setIsExpanded] = useState(false)

	//lets declare 2 variables that will describe 2 sections of our navbar.
	let RightNavOptions
	let LeftNavOptions

	//lets create a control structure which will determine the options displayed in the navbar
	if(user.email !== null) {
		RightNavOptions = (
			<Fragment>
				<Link href="/logout">
					<a className="nav-link">Logout</a>
				</Link>
			</Fragment>
		)
		LeftNavOptions = (
			<Fragment>
				<Link href="/user/categories">
					<a className="nav-link">Categories</a>
				</Link>
				<Link href="/user/records">
					<a className="nav-link">Records</a>
				</Link>
				<Link href="/user/charts/category-breakdown">
					<a className="nav-link">Breakdown</a>
				</Link>
			</Fragment>
		)
	}else {
		RightNavOptions = null
		LeftNavOptions = null
	}

   return(
   	<Fragment>
	   	<Navbar expanded={isExpanded} className="navbar" expand="lg" fixed="top" variant="dark">
	   	 <Link href="/">
	   	     <a className="navbar-brand">CashFlow</a>
	   	 </Link>
	   	 <Navbar.Toggle onClick={ () => setIsExpanded(!isExpanded) } aria-controls="basic-navbar-nav" />
	   	 <Navbar.Collapse className="basic-navbar-nav">
	        <Nav className="mr-auto" onClick={ () => setIsExpanded(!isExpanded) }>
	        	{LeftNavOptions}
	        </Nav>
	        <Nav className="ml-auto" onClick={ () => setIsExpanded(!isExpanded) } >
	        	{RightNavOptions}
	        </Nav>
	   	 </Navbar.Collapse>
	   	</Navbar>
   	</Fragment>
   	)
}